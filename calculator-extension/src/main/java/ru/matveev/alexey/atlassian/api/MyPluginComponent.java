package ru.matveev.alexey.atlassian.api;

public interface MyPluginComponent
{
    String getName();
}