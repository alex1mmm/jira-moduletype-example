package ru.matveev.alexey.atlassian.operation;

import ru.matveev.alexey.atlassian.calculator.api.Operation;

public class MinusOperation implements Operation {
    @Override
    public String getName() {
        return "minus";
    }

    @Override
    public int calculate(int val1, int val2) {
        return val1 - val2;
    }
}
