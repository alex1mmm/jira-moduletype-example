package ru.matveev.alexey.atlassian.calculator.module;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import ru.matveev.alexey.atlassian.calculator.api.Operation;


public class OperationModuleDescriptor extends AbstractModuleDescriptor<Operation>
{
    public OperationModuleDescriptor(final @ComponentImport ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public Operation getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }
}