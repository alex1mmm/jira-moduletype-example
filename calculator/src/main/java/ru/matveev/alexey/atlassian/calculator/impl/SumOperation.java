package ru.matveev.alexey.atlassian.calculator.impl;

import ru.matveev.alexey.atlassian.calculator.api.Operation;

import javax.inject.Named;

@Named
public class SumOperation implements Operation
{

    @Override
    public String getName() {
        return "sum";
    }

    @Override
    public int calculate(int val1, int val2) {
        return val1 + val2;
    }
}