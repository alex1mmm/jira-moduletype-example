package ru.matveev.alexey.atlassian.calculator.exception;

public class OperationNotFoundException extends Exception {
    public OperationNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
