package ru.matveev.alexey.atlassian.calculator.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import ru.matveev.alexey.atlassian.calculator.exception.OperationNotFoundException;
import ru.matveev.alexey.atlassian.calculator.service.CalculatorService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/calculate")
public class CalculatorRest {
    private final CalculatorService calculatorService;

    public CalculatorRest(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @POST
    @AnonymousAllowed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Response calculate(CalculatorModel body) throws OperationNotFoundException {
        int result = this.calculatorService.calculate(body.getOperation(),
                                                      body.getValue1(),
                                                      body.getValue2());
        return Response.ok(new CalculatorResponseModel(result)).build();
    }
}