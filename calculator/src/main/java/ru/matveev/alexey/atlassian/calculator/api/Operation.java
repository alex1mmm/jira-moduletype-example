package ru.matveev.alexey.atlassian.calculator.api;

public interface Operation
{
    String getName();
    int calculate(int val1, int val2);
}