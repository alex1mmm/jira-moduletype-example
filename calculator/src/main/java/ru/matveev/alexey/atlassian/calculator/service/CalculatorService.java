package ru.matveev.alexey.atlassian.calculator.service;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import ru.matveev.alexey.atlassian.calculator.api.Operation;
import ru.matveev.alexey.atlassian.calculator.exception.OperationNotFoundException;
import ru.matveev.alexey.atlassian.calculator.impl.SumOperation;
import ru.matveev.alexey.atlassian.calculator.module.OperationModuleDescriptor;

import javax.inject.Named;
import java.util.List;
import java.util.Map;

@Named
public class CalculatorService {
    private final SumOperation sumOperation;
    private final PluginAccessor pluginAccessor;
    public CalculatorService(final @ComponentImport PluginAccessor pluginAccessor,  SumOperation sumOperation) {
        this.sumOperation = sumOperation;
        this.pluginAccessor = pluginAccessor;
    }

    public int calculate(String operation, int val1, int val2) throws OperationNotFoundException {
        if (operation.equals(sumOperation.getName())) {
            return sumOperation.calculate(val1, val2);
        }
        Operation operationModule = this.getModuleForOperationName(operation);
        if (operationModule != null) {
            return operationModule.calculate(val1, val2);
        }
        throw new OperationNotFoundException(String.format("Operation %s not found", operation));
    }

    private Operation getModuleForOperationName(String operationName) {
        List<OperationModuleDescriptor> operationModuleDescriptors =
                pluginAccessor.getEnabledModuleDescriptorsByClass(OperationModuleDescriptor.class);
        for (OperationModuleDescriptor operationModuleDescriptor : operationModuleDescriptors)
        {
            if (operationName.equals(operationModuleDescriptor.getModule().getName())) {
                return operationModuleDescriptor.getModule();
            }
        }
        return null;
    }
}
