package ru.matveev.alexey.atlassian.calculator.rest;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class CalculatorResponseModel {

    @XmlElement(name = "result")
    private int result;

    public CalculatorResponseModel(int result) {
        this.result = result;
    }

}