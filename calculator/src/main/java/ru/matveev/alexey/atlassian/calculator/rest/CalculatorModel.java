package ru.matveev.alexey.atlassian.calculator.rest;

import lombok.Data;


@Data
public class CalculatorModel {

    private String operation;
    private int value1;
    private int value2;

}