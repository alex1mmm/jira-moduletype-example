package ru.matveev.alexey.atlassian.calculator.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import javax.inject.Named;

@ModuleType(ListableModuleDescriptorFactory.class)
@Named
public class BasicModuleTypeFactory extends
        SingleModuleDescriptorFactory<OperationModuleDescriptor>
{
    @Autowired
    public BasicModuleTypeFactory(HostContainer hostContainer)
    {
        super(hostContainer, "calculatorOperation", OperationModuleDescriptor.class);
    }
}
